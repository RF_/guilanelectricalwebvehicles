var mongoose    =   require('mongoose');
var Schema      =   mongoose.Schema;
var bcrypt      =   require('bcrypt');

var UserSchema   =   new Schema({


  socketID:   {
      type: String,
      required: false
  },
  name: {
      type: String,
      required: true
  } ,
  number: {
      type: String,
      unique: true,
      required: true
  },
  password: {
      type: String,
      required: true
  },
  email: {
      type: String,
      required: false
  },
  type: {
      type: String,
      required: true
  },
  lattitude: {
      type: String,
      required: false,
  },
  longtitude: {
    type: String,
    required: false,
},
messages: [ String ]
});
UserSchema.pre('save', function(next){
    var user = this;
    if(this.isModified('password')  ||  this.isNew){
        bcrypt.genSalt(10, function(err, salt){
            if(err){
                return next(err);
            }
            bcrypt.hash(manager.password, salt, function(err, hash){
                if(err){
                    return next(err);
                }
                user.password    =   hash;
                next();
            });
        });
    };
});

UserSchema.methods.comparePassword = function(pass, cb){
    bcrypt.compare(pass, this.password, function(err, isMatch){
        if(err){
            return cb(err);
        }
        cb(null, isMatch);
    });
};

module.exports  =   mongoose.model('User', UserSchema);