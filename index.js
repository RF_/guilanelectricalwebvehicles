let app         =   require('express')(),
    http        =   require('http').Server(app),
    io          =   require('socket.io')(http),

    // morgan      = require('morgan'),
    mongoose    =   require('mongoose'),
    config      =   require('./config/database'),

    // RedisDB and GEO redis
    redis       =   require('redis'),
    client      =   redis.createClient(),
    geo         =   require('georedis').initialize(client),
    testDrivers =   geo.addSet('testDrivers'),

    Manager     =   require('./app/models/manager'),
    User        =   require('./app/models/user');

var total       =   0;
var managersRoom=   'manager';
var usersRoom   =   'users';

mongoose.connect(config.database);

client.on("error", function (err) {
    console.log("Error " + err);
});

app.get('/', function(req, res) {
    res.send('Hi!What are you looking for!Give me a call 487)E9G)F,C`Q,$!G;6%I;"YC;VT` :-)');
  });

app.get('/manager', function(req, res){
    res.sendFile(__dirname + "/public/manager.html");
})

io.on('connection', (socket) => {
    total += 1;
    console.log("new user, total users", total);

    //Register New Manager for panel
    socket.on('register', ( data ) => {
        // io.emit('users-changed', {user: socket.nickname, event:})

        if( !data.username || !data.password || !data.number ){
            // Emit to user that err = true and response = 1 ( not valid username, password, number );
            socket.emit('register', {err: true, response: 1});
            console.log('adding user faild', data.username, data.password, data.number);
        }   else {
            console.log('adding user', data.username, data.password, data.number);
            var newManager  =   new Manager({
                socketID    :       socket.id,
                name        :       data.username,
                number      :       data.number,
                email       :       data.email,
                password    :       data.password
            });

            newManager.save(function(err){
                if(err){
                    // Emit to user that err = true and response = 2 ( Error while saving user );
                    console.log('mongoose error',err);
                    return socket.emit({err: true, response: 2});
                }

                // err = false and response = 0 ( user saved successfully );
                console.log('saved', data.username, data.password, data.number);
                socket.emit('register', {err: false, response: 0});
            });
        }

    });

    
    //Register Drivers
    socket.on('register-user', ( data ) => {
        // io.emit('users-changed', {user: socket.nickname, event:})

        if( !data.username || !data.password || !data.number || !data.type ){
            // Emit to user that err = true and response = 1 ( not valid username, password, number );
            socket.emit('register-user', {err: true, response: 1});
            console.log('adding user faild', data.username, data.password, data.number);
        }   else {
            console.log('adding user', data.username, data.password, data.number);
            var newUser  =   new User({
                name        :       data.username,
                number      :       data.number,
                email       :       data.email,
                password    :       data.password,
                type        :       data.type
            });

            newUser.save(function(err){
                if(err){
                    // Emit to user that err = true and response = 2 ( Error while saving user );
                    console.log('mongoose error',err);
                    return socket.emit({err: true, response: 2});
                }

                // err = false and response = 0 ( user saved successfully );
                console.log('saved', data.username, data.password, data.number);
                socket.emit('register', {err: false, response: 0});
            });
        }

    });

    //Login Manager
    socket.on('login', (data) => {
        if( !data.number || !data.password){
            // Emit to user that err = true and response = 1 ( not valid password, number );
            socket.emit('login', {err: true, response: 1});
            console.log('adding manager faild', data.password, data.number);
        }else{

            Manager.findOne({
                number: data.number
              }, function(err, manager) {
                if (err) throw err;
             
                if (!manager) {
                  socket.emit('login',{err: true, response: 'Authentication failed. Manager not found.'});
                } else {
                  // check if password matches
                  manager.comparePassword(req.body.password, function (err, isMatch) {
                    if (isMatch && !err) {
                      // if manager is found and password is right create a token
                      var token = jwt.encode(manager, config.secret);
                      // return the information including token as JSON and join to managers
                      socket.join( managersRoom) ;
                      socket.emit('login',{err: false, token: 'JWT ' + token});

                      //saving socket id in database
                      manager.socketID = socket.id;
                      manager.save(function(err){
                          if(err){
                            console.log('canont save socket id', socket.id);
                            throw err;
                          }else
                          console.log('saved socket id', socket.id);
                      });

                    } else {
                        socket.emit('login',{err: true, msg: 'Authentication failed. Wrong password.'});
                    }
                  });
                }
              });

        }
    });

    //Login User
    socket.on('login-user', (data) => {
        if( !data.number || !data.password){
            // Emit to user that err = true and response = 1 ( not valid password, number );
            socket.emit('login', {err: true, response: 1});
            console.log('adding user faild', data.password, data.number);
        }else{

            User.findOne({
                number: data.number
              }, function(err, user) {
                if (err) throw err;
             
                if (!user) {
                  socket.emit('fail',{err: true, response: 'Authentication failed. User not found.'});
                } else {
                  // check if password matches
                  user.comparePassword(req.body.password, function (err, isMatch) {
                    if (isMatch && !err) {
                      // if user is found and password is right create a token
                      var token = jwt.encode(user, config.secret);
                      // return the information including token as JSON
                      socket.emit('login',{err: false, token: 'JWT ' + token});
                      io.sockets.in(managersRoom).emit('login-user', {number: data.number, response: 'User loged in successfully'});
                    } else {
                        socket.emit('fail',{err: true, msg: 'Authentication failed. Wrong password.'});
                    }
                  });
                }
              });

        }
    });


    //Setting destiny for drivers
    socket.on('setDestiny', (data) => {
        if( !data.number || !data.lattitude || !data.longtitude){
            socket.emit('fail', {err: true, response: 1});
            console.log('setting Destiny faild', data.number, data.lattitude, data.longtitude);
        }else{
            User.findOne({
                number: data.number
            }, function(err, user)  {
                if(err) throw err;
                if (!user)  {
                    socket.emit('fail',{err: true, response: 'User not found.'});
                }else{
                    user.lattitude  =   data.lattitude;
                    user.longtitude =   data.longtitude; 
                    user.save(function(err){
                        if(!err){
                            console.log("setting destiny done");
                            socket.emit("setDestiny",{err: false, response: "Destiny have been set."});
                        }else{
                            console.log("setting destiny faild");
                            socket.emit("fail",{err: false, response: "ERR response."+ err});
                        }
                    });
                }
            });
        }
    });

    socket.on('send-notification', (data) => {
        if( !data.number || !data.message){
            console.log("faild to send message for lake of data", data.number, data.message);
            socket.emit('faild', {err: true, response: 'number and message required'});
        }else{
            User.findOne({
                number: data.number
            }, function(err, user){
                if(err){
                    console.log('faild to send message by error', err);
                    socket.emit('faild', {err: true, response: err});
                }else{
                    console.log('successfully got the user to send massage', data.number);
                    user.messages.unshift(data.message);
                }
            });
        }
    });


    //save users lattitude and longtitude
    socket.on('set-user-location', (data) => {
        if(!data.number || !data.lattitude || !data.longtitude || !data.type ){
            console.log('setting user location faild not efficient args', data.number, data.lattitude, data.longtitude, data.type);
            socket.emit('faild', {err: true, response: 'not efficient data'});
        }else{
            switch (data.type){
                case 0:
                    addTestDirverLocation(data.number, data.lattitude, data.longtitude);
                    break;
                case 1:
                    //addMore driver methods and variables based on usage
                    break;
                //..
            }
        }
    });

    //Retriving user location
    socket.on('get-user-location', (data) => {
        if(!data.number){
            socket.emit('faild', {err: true, response: 'not efficiend data'});
        }else{
            User.findOne({
                number: data.number
            }, function(err, user){
                if(err) throw err;
                else {
                    switch (user.type){
                        case 'test':
                            getTestDriverLocation(data.number, function(err, location){
                                if(err)
                                    socket.emit('faild', {err: true, response: err});
                                else
                                    socket.emit('location', {err: false, response: "found user location", body: location, number: data.number, type: user.type});
                            });
                    }
                }
            });
        }
    });

    //Retrive all the locations
    socket.on('get-all-locations', (data) => {
        
        User.find({}, 'number', function(err, array){
            if(err){
                console.log('getting all the drivers of testDrivers set number for location faild', err);
                socket.emit('faild', {err: true, response: err});
            }else
                testDrivers.location(array, function(err, locations){
                    if(err){
                        console.log('getting all the locations of dirver test set faild', err);
                        socket.emit('faild', {err: true, response: err});
                    }else{
                        console.log('Got every drivers location', reply);
                        socket.emit('locations', {err: false, response: "Locations found", body: locations});
                    }
                });
        });
        testDrivers.locations()
    });

});

//END OF SOCKET ROUTS

//METHODS

//Add test driver type location 
function addTestDirverLocation(id, lattitude, longtitude){
    testDrivers.location(id, function(err, location){
        if(err) throw err;
        else {
            if(location != null)
                testDrivers.updateLocation(data.number, {lattitude: lattitude, longtitude: longtitude}, function(err, reply){
                    if(err) throw err;
                    else console.log('Updating test driver location : DONE');
                });
            else 
            testDrivers.addLocation(data.number, {lattitude: lattitude, longtitude: longtitude}, function(err, reply){
                if(err) throw err;
                else console.log('Adding test driver location: DONE');
            })

        }
    })
}

function getTestDriverLocation(id, callback){
    testDrivers.location(id, function(error, location){
        if(error){
            console.log('getting user location error, not efficient data', error);
            callback(true, null)
        }else{
            callback(false, location);
        }
    })
}

var port = process.env.PORT || 3001;

http.listen(port, function(){
  console.log('listening in http://localhost:' + port);
});